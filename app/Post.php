<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Concerns\HasTimestamps;
use Jenssegers\Mongodb\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\SoftDeletes;
use Jenssegers\Mongodb\Eloquent\Builder;
use League\CommonMark\CommonMarkConverter;

class Post extends Model
{
    use HasTimestamps;
    use SoftDeletes;

    protected $connection = 'mongodb';
    protected $collection = 'posts';

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
        'post_at',
    ];

    protected $primaryKey = 'slug';

    protected $fillable = [
        'title',
        'slug',
        'content',
        'post_at',
        'banner',
    ];

    public static function boot()
    {
        parent::boot();

        static::addGlobalScope(function(\Illuminate\Database\Eloquent\Builder $builder) {
            $builder->orderBy('post_at', 'desc');
        });

        static::saving(function (self $model) {
            if ($model->slug === null) {
                $model->slug = str_slug($model->title);
            }

            if ($model->post_at === null) {
                $model->post_at = Carbon::now();
            }
        });
    }

    /**
     * Convert the user input into HTML from Markdown, and save alongside
     * @param string $value
     */
    public function setContentAttribute(string $value)
    {
        $this->attributes['markdown'] = $value;

        $this->attributes['html'] = (new CommonMarkConverter(['html_input' => 'escape']))->convertToHtml($value);
    }

    public function getContentAttribute()
    {
        return $this->attributes['markdown'] ?? null;
    }

    public function scopePublished(Builder $query)
    {
        $query->where('post_at', '<=', Carbon::now());
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
