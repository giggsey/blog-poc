<?php

namespace App\Http\Controllers;

use App\Post;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Post::published()->has('user')->with('user')->paginate(5);

        return view('pages/posts/index', ['posts' => $posts]);
    }

    public function show($slug)
    {
        $post = Post::published()->findOrFail($slug);

        return view('pages/posts/show', ['post' => $post]);
    }
}
