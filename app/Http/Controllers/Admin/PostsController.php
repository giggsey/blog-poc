<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\PostRequest;
use App\Post;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;

class PostsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Post::has('user')->with('user')->paginate();

        return view('pages/admin/posts/index', ['posts' => $posts]);
    }

    public function randomImage()
    {
        /*
         * Need to access the URL, then return the URL after redirects
         */
        $url = 'https://picsum.photos/1000/400/?random';

        $client = app(Client::class);

        try {
            $response = $client->get($url);

            $file = 'images/' . Str::random(16) . '.jpg';

            \Storage::disk('public')->put($file, $response->getBody(), 'public');

            return [
                'url' => \Storage::disk('public')->url($file),
            ];
        } catch (RequestException $e) {
            logger()->warning('Unable to fetch URL due to error', ['exception' => $e]);
            abort(500, 'Unable to get image at this time');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages/admin/posts/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(PostRequest $request)
    {
        $this->validate($request, [
            'slug' => [Rule::unique('mongodb.posts', 'slug')],
        ]);

        if ($request->has('file')) {
            $file = \Storage::disk('public')->putFile('images', $request->file('file'), 'public');
            $path = \Storage::disk('public')->url($file);
        } else {
            // Use the URL instead
            $path = $request->input('url');
        }

        $post = new Post();
        $post->fill([
            // We don't want a straight $request->all(), as we want to rename some parameters
            'title' => $request->input('title'),
            'slug' => $request->input('slug'),
            'content' => $request->input('post'),
            'post_at' => $request->input('postDate'),
            'banner' => $path,
        ]);
        $post->user()->associate(\Auth::user());

        $post->save();

        return redirect()->route('admin.posts.index')->with('message', "Post has been created");
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Post $post
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post)
    {
        return view('pages/admin/posts/show', ['post' => $post]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Post $post
     * @return \Illuminate\Http\Response
     */
    public function edit(Post $post)
    {
        return view('pages/admin/posts/edit', ['post' => $post]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Post $post
     * @return \Illuminate\Http\Response
     */
    public function update(PostRequest $request, Post $post)
    {
        $this->validate($request, [
            'slug' => [Rule::unique('mongodb.posts', 'slug')->ignore($post->_id, '_id')],
        ]);

        if ($request->has('file')) {
            $file = \Storage::disk('public')->putFile('images', $request->file('file'), 'public');
            $path = \Storage::disk('public')->url($file);
        } else {
            // Use the URL instead
            $path = $request->input('url');
        }

        $post->fill([
            // We don't want a straight $request->all(), as we want to rename some parameters
            'title' => $request->input('title'),
            'slug' => $request->input('slug'),
            'content' => $request->input('post'),
            'post_at' => $request->input('postDate'),
            'banner' => $path,
        ]);

        $post->save();

        return redirect()->route('admin.posts.index')->with('message', "Post has been updated");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Post $post
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $post)
    {
        $post->delete();

        return redirect()->route('admin.posts.index')->with('message', "Post has been deleted");
    }
}
