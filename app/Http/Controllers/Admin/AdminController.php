<?php
/**
 *
 * @author giggsey
 * @package blog
 */

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Post;
use App\User;

class AdminController extends Controller
{
    public function index()
    {
        $posts = Post::has('user')->count();

        $users = User::count();

        return view('pages.admin.index', ['users' => $users, 'posts' => $posts]);
    }
}
