<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => ['required', 'string'],
            'slug' => ['nullable', 'alpha_dash', 'max:100'],
            'post' => ['required', 'string'],
            'postDate' => ['nullable', 'date'],
            'url' => ['nullable', 'url', 'required_without:file'],
            'file' => ['nullable', 'file'],
        ];
    }
}
