<?php
/**
 *
 * @author giggsey
 * @package blog
 */

use App\User;
use Faker\Generator as Faker;
use GuzzleHttp\Client;
use Illuminate\Support\Str;

$factory->define(App\Post::class, function (Faker $faker) {

    // Save a random image

    $url = 'https://picsum.photos/1000/400/?random';
    $client = app(Client::class);

    $response = $client->get($url);

    $file = 'images/' . Str::random(16) . '.jpg';
    \Storage::disk('public')->put($file, $response->getBody(), 'public');

    $url = \Storage::disk('public')->url($file);

    return [
        'title' => $faker->unique()->sentence(6),
        'banner' => $url,
        'slug' => $faker->unique()->slug,
        'post_at' => $faker->dateTimeBetween(\Carbon\Carbon::now()->subMonths(3), \Carbon\Carbon::now()->subDay(3)),
        'content' => $faker->paragraphs(4, true),
        'user_id' => User::all()->random()->id,
    ];
});
