<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // Generate a bunch of random posts
        factory(App\User::class, 5)->create();
        factory(App\Post::class, 21)->create();

        $user = \App\User::findOrNew('example@example.com');
        $user->name = 'Joshua Gigg';
        $user->email = 'example@example.com';
        $user->password = Hash::make('secret');
        $user->save();

        $post = \App\Post::findOrNew('getting-started');
        $post->user()->associate($user);
        $post->title = 'Getting Started';
        $post->post_at = \Carbon\Carbon::now();
        $post->banner = 'https://i.imgur.com/pQjPibs.jpg';
        $post->content = <<<doc
Welcome to a very simple blog.

To get started, you'll need to [login](/login).

An example user has been created, with the following credentials:
 - Email: `{$user->email}`
 - Password: `secret`

Once logged in, you can create posts by visiting the Admin section, linked at the bottom of the page. 
doc;

        $post->save();
    }
}
