@extends('layouts.app')

@section('title')
    Create Post
@endsection

@section('content')

    <h3>Create a new post</h3>

    @include('components.breadcrumbs', ['links' => [
        'Admin' => route('admin.index'),
        'Posts' => route('admin.posts.index'),
        'Create' => route('admin.posts.create'),
    ]])

    @include('components.errors')

    @include('components.post-form', ['url' => route('admin.posts.store')])
@endsection
