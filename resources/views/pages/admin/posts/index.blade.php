@extends('layouts.app')

@section('content')
    @include('components.messages')

    <h2>Blog Posts</h2>

    @include('components.breadcrumbs', ['links' => [
        'Admin' => route('admin.index'),
        'Posts' => route('admin.posts.index'),
    ]])

    <a href="{{route('admin.posts.create')}}" class="btn btn-primary mb-3">Create Post</a>

    @if (count($posts) > 0)
        <table class="table table-bordered table-striped">
            <thead>
            <tr>
                <th>Title</th>
                <th>Post Date</th>
                <th>Author</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            @foreach ($posts as $post)
                <tr>
                    <td><a href="{{route('admin.posts.show', $post)}}">{{$post->title}}</a></td>
                    <td
                        @if ($post->post_at > \Illuminate\Support\Carbon::now())
                        class="bg-info"
                        title="Post not yet published"
                        @endif
                    >{{$post->post_at}}</td>
                    <td><a href="{{route('admin.users.show', $post->user)}}">{{$post->user->name}}</a></td>
                    <td>
                        <a href="{{ route('admin.posts.edit', $post) }}" class="btn btn-outline-primary">Edit</a>
                        <form action="{{route('admin.posts.destroy', $post)}}" method="post" class="d-inline-block">
                            {{-- @todo Confirmation --}}
                            {{ method_field('delete') }}
                            {{ csrf_field() }}
                            <input type="submit" value="Delete" class="btn btn-outline-danger">
                        </form>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        {{ $posts->links() }}
    @else
        <div class="alert alert-info">No posts yet, why not <a href="{{route('admin.posts.create')}}">create</a> one?
        </div>
    @endif
@endsection
