@extends('layouts.app')

@section('title')
    Admin &raquo; Posts &raquo; {{$post->title}}
@endsection

@section('content')
    @include('components.messages')

    @include('components.breadcrumbs', ['links' => [
        'Admin' => route('admin.index'),
        'Posts' => route('admin.posts.index'),
        $post->title => route('admin.posts.show', $post),
    ]])

    <div class="row mb-3">
        <div class="col-6 text-left">
            <a href="{{route('admin.posts.index')}}" class="btn btn-outline-primary">Back to all posts</a>
        </div>
        <div class="col-6 text-right">
            <a href="{{ route('admin.posts.edit', $post) }}" class="btn btn-outline-primary">Edit</a>
            <form action="{{route('admin.posts.destroy', $post)}}" method="post" class="d-inline-block">
                {{-- @todo Confirmation --}}
                {{ method_field('delete') }}
                {{ csrf_field() }}
                <input type="submit" value="Delete" class="btn btn-outline-danger">
            </form>
        </div>
    </div>

    @include('components.post')
@endsection
