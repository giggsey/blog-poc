@extends('layouts.app')

@section('title')
    Edit Post &raquo; {{$post->title}}
@endsection

@section('content')

    <h3>Edit post: {{$post->title}}</h3>

    @include('components.breadcrumbs', ['links' => [
        'Admin' => route('admin.index'),
        'Posts' => route('admin.posts.index'),
        $post->title => route('admin.posts.edit', $post),
    ]])

    @include('components.errors')

    @include('components.post-form', ['url' => route('admin.posts.update', $post), 'method' => 'patch'])
@endsection
