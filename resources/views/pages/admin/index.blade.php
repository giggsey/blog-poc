@extends('layouts.app')

@section('title')
    Admin
@endsection

@section('content')
    @include('components.messages')

    <h1>Admin</h1>

    @include('components.breadcrumbs', ['links' => [
        'Admin' => route('admin.index'),
    ]])

    <div class="card-deck admin-cards">
        <a href="{{route('admin.users.index')}}">
            <div class="card text-white bg-primary mb-3">
                <div class="card-body">
                    <h5 class="card-title">Users</h5>
                    <p class="card-text">{{$users}}</p>
                </div>
            </div>
        </a>

        <a href="{{route('admin.posts.index')}}">
            <div class="card text-white bg-info mb-3">
                <div class="card-body">
                    <h5 class="card-title">Posts</h5>
                    <p class="card-text">{{$posts}}</p>
                </div>
            </div>
        </a>

    </div>


@endsection
