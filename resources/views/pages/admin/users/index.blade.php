@extends('layouts.app')

@section('content')
    @include('components.messages')

    <h2>Users</h2>

    @include('components.breadcrumbs', ['links' => [
        'Admin' => route('admin.index'),
        'Users' => route('admin.users.index'),
    ]])

    <a href="{{route('admin.users.create')}}" class="btn btn-primary mb-3">Create User</a>

    @if (count($users) > 0)
        <table class="table table-bordered table-striped">
            <thead>
            <tr>
                <th>Name</th>
                <th>Email Address</th>
                <th>Created</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            @foreach ($users as $user)
                <tr>
                    <td><a href="{{route('admin.users.show', $user)}}">{{$user->name}}</a></td>
                    <td>{{$user->email}}</td>
                    <td>{{$user->created_at}}</td>
                    <td>
                        <a href="{{ route('admin.users.edit', $user) }}" class="btn btn-outline-primary">Edit</a>
                        @if (Auth::user() != $user)
                            <form action="{{route('admin.users.destroy', $user)}}" method="post" class="d-inline-block">
                                {{-- @todo Confirmation --}}
                                {{ method_field('delete') }}
                                {{ csrf_field() }}
                                <input type="submit" value="Delete" class="btn btn-outline-danger">
                            </form>
                        @endif
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        {{ $users->links() }}
    @else
        <div class="alert alert-info">No users yet, so how did you log in?</div>
    @endif
@endsection
