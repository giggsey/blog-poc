@extends('layouts.app')

@section('title')
    Create User
@endsection

@section('content')

    <h3>Create user</h3>

    @include('components.breadcrumbs', ['links' => [
        'Admin' => route('admin.index'),
        'Users' => route('admin.users.index'),
        'Create' => route('admin.users.create'),
    ]])

    @include('components.errors')

    @include('components.user-form', ['url' => route('admin.users.store'), 'method' => 'post', 'action' => 'create'])
@endsection
