@extends('layouts.app')

@section('title')
    Edit User &raquo; {{$user->name}}
@endsection

@section('content')

    <h3>Edit user: {{$user->name}}</h3>

    @include('components.breadcrumbs', ['links' => [
        'Admin' => route('admin.index'),
        'Users' => route('admin.users.index'),
        $user->name => route('admin.users.edit', $user),
    ]])

    @include('components.errors')

    @include('components.user-form', ['url' => route('admin.users.update', $user), 'method' => 'patch', 'action' => 'edit'])
@endsection
