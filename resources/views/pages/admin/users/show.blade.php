@extends('layouts.app')

@section('title')
    Admin &raquo; Users &raquo; {{$user->name}}
@endsection

@section('content')
    @include('components.messages')

    @include('components.breadcrumbs', ['links' => [
        'Admin' => route('admin.index'),
        'Users' => route('admin.users.index'),
        $user->name => route('admin.users.show', $user),
    ]])

    <div class="row mb-3">
        <div class="col-6 text-left">
            <a href="{{route('admin.users.index')}}" class="btn btn-outline-primary">Back to all users</a>
        </div>
        <div class="col-6 text-right">
            <a href="{{ route('admin.users.edit', $user) }}" class="btn btn-outline-primary">Edit</a>
            @if (Auth::user() != $user)
                <form action="{{route('admin.users.destroy', $user)}}" method="post" class="d-inline-block">
                    {{-- @todo Confirmation --}}
                    {{ method_field('delete') }}
                    {{ csrf_field() }}
                    <input type="submit" value="Delete" class="btn btn-outline-danger">
                </form>
            @endif
        </div>
    </div>

    <table class="table table-bordered">
        <tbody>
        <tr>
            <th>Name</th>
            <td>{{$user->name}}</td>
        </tr>
        <tr>
            <th>Email</th>
            <td>{{$user->email}}</td>
        </tr>
        <tr>
            <th>Created on</th>
            <td>{{$user->created_at}}</td>
        </tr>
        </tbody>
    </table>

    <h3>Posts by {{$user->name}}</h3>

    @if (count($user->posts) > 0)
        <table class="table table-bordered table-striped">
            <thead>
            <tr>
                <th>Title</th>
                <th>Post Date</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($user->posts as $post)
                <tr>
                    <td><a href="{{route('admin.posts.show', $post)}}">{{$post->title}}<a/></td>
                    <td>{{$post->post_at}}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    @else
        <div class="alert alert-info">This user hasn't created any posts yet</div>
    @endif
@endsection
