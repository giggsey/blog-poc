@extends('layouts.app')

@section('content')
    @if (count($posts) > 0)
        @foreach ($posts as $post)
            @include('components.post', ['link' => route('posts.show', $post)])
        @endforeach

        {{ $posts->links() }}
    @else
        <div class="alert alert-info">It looks like we haven't posted anything yet. Be sure to come back soon.</div>
    @endif
@endsection
