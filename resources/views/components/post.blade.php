<div class="card mb-3">

    <div class="header-image" style="background-image: url('{{$post->banner}}')">
        <div class="header-text">
            <h2 class="card-title">
                @if (isset($link))
                    <a href="{{$link}}">
                        {{$post->title}}
                    </a>
                @else
                    {{$post->title}}
                @endif
            </h2>
            <p>{{$post->post_at}} - {{$post->user->name}}</p>
        </div>
    </div>
    <div class="card-body">
        <p class="card-text">{!! $post->html !!}</p>
    </div>
</div>
