<form action="{{$url}}" method="post" enctype="multipart/form-data">
    {{ csrf_field() }}
    {{ method_field($method ?? 'post') }}

    <div class="form-group">
        <label for="input-title">Title</label>
        <input type="text" class="form-control" id="input-title" placeholder="Blog post title" name="title"
               required value="{{old('title', $post->title ?? null)}}">
    </div>

    <div class="form-group">
        <label for="input-slug">URL Slug</label>
        <input type="text" class="form-control" id="input-slug" placeholder="blog-post-title" name="slug"
               aria-describedby="slug-help" value="{{old('slug', $post->slug ?? null)}}">
        <small id="slug-help" class="form-text text-muted">Leave this blank to generate automatically</small>
    </div>

    <div class="form-group">
        <label for="input-banner">Banner Image</label>
        <small id="banner-help" class="form-text text-muted">Specify a URL or upload an image</small>

        <div class="row">
            <div class="col-5">
                <input type="text" aria-describedby="banner-help" class="form-control"
                       id="input-banner" placeholder="http://example.com/image.png" name="url"
                       value="{{old('url', $post->banner ?? null)}}">
                or
                <input type="file" aria-describedby="banner-help" class="form-control-file"
                       placeholder="Select a file to upload" name="file" value="{{old('file')}}">

                <button class="btn btn-sm btn-outline-info mt-2" id="random-image">Need inspiration? Random image
                </button>
            </div>

            <div class="col-7">
                <small>Preview</small>
                <img id="preview-image" src="{{old('url', $post->banner ?? null)}}" class="img-fluid" style="max-height: 300px">
            </div>
        </div>
    </div>

    <div class="form-group">
        <label for="input-post">Post</label>
        <textarea class="form-control" id="input-post" rows="10" name="post"
                  required>{{old('post', $post->content ?? null)}}</textarea>
        <small id="post-help" class="form-text text-muted">Accepts markdown</small>
    </div>

    <div class="form-group">
        <label for="input-post-date">Post Date</label>
        <input type="text" class="form-control daterange" id="input-post-date" placeholder="Now" name="postDate"
               aria-describedby="post-date-help" value="{{old('postDate', $post->post_at ?? null)}}">
        <small id="post-date-help" class="form-text text-muted">When do you want this post to be made public? Leave
            empty for right now.
        </small>
    </div>


    <button type="submit" class="btn btn-primary">Submit</button>

</form>


@section('javascript')
    <script type="text/javascript">
        $().ready(function () {
            $("input[name='url']").change(function () {
                $("#preview-image").attr('src', $(this).val());
            });

            $("input[name='file']").change(function () {
                // @see https://stackoverflow.com/a/4459419/403165
                if (this.files && this.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function (e) {
                        $('#preview-image').attr('src', e.target.result);
                    };

                    reader.readAsDataURL(this.files[0]);
                }
            });

            $("#random-image").click(function (e) {
                var that = $(this);
                var oldContent = that.text();
                that.text('Please wait...');
                that.attr('disabled', true);
                e.preventDefault();

                $.getJSON(@json(route('admin.images.random')), function (d) {
                    $("input[name='url']").val(d.url).change();

                    that.attr('disabled', false);
                    that.text(oldContent);
                });
            })
        });
    </script>
@endsection
