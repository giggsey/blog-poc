<form action="{{$url}}" method="post">
    {{ csrf_field() }}
    {{ method_field($method ?? 'post') }}

    <div class="form-group">
        <label for="input-name">Name</label>
        <input type="text" class="form-control" id="input-name" placeholder="Name" name="name"
               required value="{{old('name', $user->name ?? null)}}">
    </div>

    <div class="form-group">
        <label for="input-email">Email Address</label>
        <input type="email" class="form-control" id="input-email" placeholder="email@example.com" name="email"
               required value="{{old('email', $user->email ?? null)}}">
    </div>

    <div class="form-group">
        <label for="input-password">
            @if ($action === 'edit')
                New
            @endif
            Password</label>
        <input type="password" class="form-control" id="input-password" placeholder="Password" name="password">
    </div>

    <div class="form-group">
        <label for="input-confirm-password">Confirm Password</label>
        <input type="password" class="form-control" id="input-confirm-password" placeholder="Password" name="password_confirmation" aria-describedby="password-confirmation-help">
        <small id="password-confirmation-help" class="form-text text-muted">Please repeat the password to confirm.</small>
    </div>

    <button type="submit" class="btn btn-primary">Submit</button>

</form>
