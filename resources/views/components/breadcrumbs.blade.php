<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        @foreach ($links as $title => $link)
            @if ($loop->last)
                <li class="breadcrumb-item active" aria-current="page">{{$title}}</li>
            @else
                <li class="breadcrumb-item"><a href="{{$link}}">{{$title}}</a></li>
            @endif
        @endforeach
    </ol>
</nav>
