@if ($errors->any())
    <div class="alert alert-danger">
        <strong>Sorry, but there has been
            @if (count($errors) === 1)
                an
            @endif
            {{str_plural('error', count($errors))}}:</strong>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
