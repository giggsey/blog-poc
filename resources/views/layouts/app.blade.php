<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@section('title')
            My Blog
        @show</title>

    <link href="{{mix('css/app.css')}}" rel="stylesheet" type="text/css">
</head>
<body>
<div class="container">
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <a class="navbar-brand" href="{{route('posts.index')}}">My Blog</a>
    </nav>


    @yield('content')


    <footer>
        <div class="float-left">
            &copy; MyBlog 2018
        </div>
        <div class="float-right">
            @guest
                <a href="{{route('login')}}">Login</a>
            @else
                Logged in as {{Auth::user()->name}} &middot;
                <a href="{{route('admin.index')}}">Admin</a> &middot;
                <a href="{{route('logout')}}"
                   onclick="event.preventDefault(); document.getElementById('frm-logout').submit();">Logout</a>
                <form id="frm-logout" action="{{ route('logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                </form>
            @endif
        </div>
    </footer>
</div>

<script src="{{ mix('js/app.js') }}" type="text/javascript"></script>

@yield('javascript')

</body>
</html>
