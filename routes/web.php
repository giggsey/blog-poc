<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// Auth
Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');
Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
Route::post('password/reset', 'Auth\ResetPasswordController@reset');

Route::namespace('Admin')
    ->name('admin.')
    ->prefix('admin')
    ->middleware('auth')
    ->group(function () {
        Route::get('/', 'AdminController@index')->name('index');

        Route::resource('posts', 'PostsController');
        Route::resource('users', 'UsersController');

        Route::get('images/random', 'PostsController@randomImage')->name('images.random'); // @todo Move to AdminController
    });

Route::get('/', 'PostController@index')->name('posts.index');
Route::get('/{post}', 'PostController@show')->name('posts.show');
