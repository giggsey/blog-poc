# Blog

## Getting Started

Make sure you have all the dependencies & build the frontend

```bash
composer install
npm install
npm run production
```

## Database

The project currently uses MySQL for User storage, and MongoDB for storing the posts.

Make sure you configure the .env with the correct `DB_*` and `MONGO_DB_*` values.

### Migrations & Seed

Seed data exists for this project, so you'll need to run the migrations and seed any data.

**Important**: Due to loading 'fake' images, make sure your APP_URL is set correctly before you seed.

```bash
php artisan migrate --seed
```
Once seeded, there will be an Example post on the blog with login credentials.

## TODO
 - Simple side bar (search + tags)
 - Comments on posts
